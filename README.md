# Animo PHP Buildah SDK

A working concept of a CI worker built using PHP and buildah. With no docker in docker!

```
docker-compose build ci
docker-compose up composer
# wait for composer dependancies to install...

# this command will run the contents of tests/index.php
# it will create a git container, pull down laravel in to the mounted .app folder
# another composer container will then mount the .app and run composer install
# all with no docker-in-docker...
docker-compose up ci

```

Using in production:

```
docker run -v $(pwd)/spec/animo-ci.yml:/spec/animo-ci.yml -v $(pwd)/.builds:/artifacts --privileged animocloud/ci
```

Using `-v $(pwd)/.builds:/artifacts` will mount any outputs you create.

To interact with the run command add the `-it` argument.
