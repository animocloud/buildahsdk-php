<?php

use Animo\BuildahSDK\BuildahCiFactory;

require_once __DIR__ . "/../vendor/autoload.php";

BuildahCiFactory::fromYaml(__DIR__ ."/../spec/animo-ci.yml");
