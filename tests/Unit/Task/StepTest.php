<?php

namespace Tests\Unit\Task;

use Animo\BuildahSDK\Task\Step;
use Symfony\Component\Process\Process;
use Tests\Unit\TestCase;

class StepTest extends TestCase {

    private $solidClass;

    protected function setUp(): void
    {
        parent::setUp();

        $this->solidClass = \Mockery::mock(Step::class)->makePartial();
        $this->solidClass->shouldReceive('asString')->andReturn('a string');
    }

    public function testGetTimeout()
    {
        $timeout = $this->solidClass->getTimeout();

        $this->assertIsInt($timeout);
        $this->assertSame(60, $timeout);
    }

    public function testSetTimeout()
    {
        $initialTimeout = $this->solidClass->getTimeout();

        $this->solidClass->setTimeout(300);
        $timeout = $this->solidClass->getTimeout();

        $this->assertSame(60, $initialTimeout);

        $this->assertIsInt($timeout);
        $this->assertSame(300, $timeout);
    }

    public function testGetProcess()
    {
        $this->expectException(\Exception::class);
        $this->solidClass->getProcess();
    }

    public function testSetProcess()
    {
        $this->solidClass->setProcess(new Process([]));
        $this->assertInstanceOf(Process::class, $this->solidClass->getProcess());
    }

    public function test__toString()
    {
        $str = (string) $this->solidClass;
        $this->assertIsString($str);
        $this->assertSame('a string', $str);
    }
}
