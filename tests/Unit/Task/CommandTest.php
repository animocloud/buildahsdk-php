<?php

namespace Tests\Unit\Task;

use Animo\BuildahSDK\BuildahContainer;
use Animo\BuildahSDK\Task\Command;
use Tests\Unit\TestCase;

class CommandTest extends TestCase {

    public function test__construct()
    {
        $command = new Command('echo hello world');
        $this->assertInstanceOf(Command::class, $command);
    }

    public function testAsString()
    {
        $cmdString = 'echo hello world';
        $command = new Command($cmdString);

        $this->assertSame($cmdString, $command->asString());
        $this->assertIsString($command->asString());
    }

    public function testGetRunCommand()
    {
        $container = new BuildahContainer('alpine', 'test-container');
        $cmdString = 'echo hello world';
        $command = new Command($cmdString);

        $this->assertSame($cmdString, $command->getRunCommand($container));
        $this->assertIsString($command->getRunCommand($container));
    }
}
