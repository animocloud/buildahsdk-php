<?php

namespace Tests\Unit\Task;

use Animo\BuildahSDK\BuildahContainer;
use Animo\BuildahSDK\Task\Script;
use Tests\Unit\TestCase;

class ScriptTest extends TestCase {

    public function test__construct()
    {
        $script = new Script();
        $this->assertInstanceOf(Script::class, $script);
    }

    public function testAddLine()
    {
        $script = new Script();
        $script->addLine('123');

        $this->assertArrayHasKey(0, $script->getLines());

        $lines = $script->getLines();
        $this->assertSame('123', $lines[0]);
    }

    public function testAddLines()
    {
        $script = new Script();
        $script->addLine('123');
        $script->addLine('abc');

        $lines = $script->getLines();

        $this->assertCount(2, $lines);
        $this->assertArrayHasKey(0, $lines);
        $this->assertArrayHasKey(1, $lines);

        $this->assertSame('123', $lines[0]);
        $this->assertSame('abc', $lines[1]);
    }

    public function testAsString()
    {
        $script = new Script();
        $script->addLine('123');
        $script->addLine('abc');

        $this->assertSame("123\nabc\n", $script->asString());
        $this->assertIsString($script->asString());
    }

    public function testGetRunCommand()
    {
        $container = new BuildahContainer('alpine', 'test-container');
        $script = new Script();
        $script->addLine('123');
        $script->addLine('abc');

        $runCommand = $script->getRunCommand($container);
        $this->assertSame("/scripts/run.sh", $runCommand);
        $this->assertIsString($runCommand);
    }
}
