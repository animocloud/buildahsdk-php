<?php

namespace Tests\Unit;

use Animo\BuildahSDK\BuildahCmd;
use Animo\BuildahSDK\BuildahContainer;
use Animo\BuildahSDK\BuildahTask;
use Animo\BuildahSDK\Pipeline;

class PipelineTest extends TestCase {

    private function newPipeline()
    {
        return new Pipeline($this->getMockedBuildahCmd());
    }

    public function test__constructor()
    {
        $pipeline = $this->newPipeline();
        $this->assertInstanceOf(Pipeline::class, $pipeline);
    }

    public function testGitTask()
    {
        $pipeline = $this->newPipeline();

        $this->assertInstanceOf(Pipeline\Git::class, $pipeline->gitTask('git'));
    }

    public function testTask()
    {
        $pipeline = $this->newPipeline();

        $task = $pipeline->task('test task', function () {}, new BuildahContainer('alpine', 'task-test'));

        $this->assertInstanceOf(Pipeline::class, $task);
        $this->assertNotNull($pipeline->getTask('test task'));
    }

    public function testAddTask()
    {
        $pipeline = $this->newPipeline();

        $pipeline->addTask('add task test', new BuildahTask($pipeline));
        $this->assertNotNull($pipeline->getTask('add task test'));
    }

    public function testGetTask()
    {
        $pipeline = $this->newPipeline();
        $this->assertNull($pipeline->getTask('no task here'));
    }

    public function testExecute()
    {
        $pipeline = $this->newPipeline();
        $testContainer = new BuildahContainer('alpine', 'alpine-test');

        $pipeline->task('test execution', function (BuildahTask $task) {

        }, $testContainer);

        $pipeline->execute();

        $this->assertTrue($testContainer->hasStarted());
        $this->assertTrue($pipeline->getTask('test execution')->hasExecuted());
    }
}
