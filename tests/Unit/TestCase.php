<?php

namespace Tests\Unit;

use Animo\BuildahSDK\BuildahCmd;
use PHPUnit\Framework\TestCase as PHPUnitTestCase;
use Symfony\Component\Process\Process;

class TestCase extends PHPUnitTestCase {

    protected $processMock;

    protected $buildahCmdMock;

    protected $successfulExecution = false;

    protected function setUp(): void
    {
        parent::setUp();

        // $this->processMock = \Mockery::mock(Process::class)->makePartial();
        // $this->processMock->shouldReceive('run')->andReturn(0);
        // $this->processMock->shouldReceive('isSuccessful')->andReturn($this->successfulExecution);

        // $this->buildahCmdMock = \Mockery::mock(BuildahCmd::class)->makePartial();
        // $this->buildahCmdMock->shouldAllowMockingProtectedMethods();
        // $this->buildahCmdMock->shouldReceive('newProcess')->andReturn($this->processMock);
    }

    protected function getMockedProcess($successfulExecution = false)
    {
        $processMock = \Mockery::mock(Process::class)->makePartial();
        $processMock->shouldReceive('run')->andReturn(0);
        $processMock->shouldReceive('isSuccessful')->andReturn($successfulExecution);

        return $processMock;
    }

    protected function getMockedBuildahCmd($successfulProcess = true)
    {
        $buildahCmdMock = \Mockery::mock(BuildahCmd::class)->makePartial();
        $buildahCmdMock->shouldAllowMockingProtectedMethods();
        $buildahCmdMock->shouldReceive('newProcess')->andReturn($this->getMockedProcess($successfulProcess));

        return $buildahCmdMock;
    }
}
