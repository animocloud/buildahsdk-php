<?php

use Animo\BuildahSDK\Containerfile;
use Animo\BuildahSDK\Containerfile\Instructions;

require_once __DIR__ . "/../vendor/autoload.php";

$buildahCmd = new \Animo\BuildahSDK\BuildahCmd(['--volume=/app:/buildah']);
// Cleanup from previous jobs; delete old containers and images
$buildahCmd->cleanUp();

$pipeline = new \Animo\BuildahSDK\Pipeline($buildahCmd);

$gitContainer = new \Animo\BuildahSDK\BuildahContainer(
    'alpine/git',
    'git-container'
);

// Start the git container with a task (you could inject a private key here to pull a private repo)
$gitContainer->start($buildahCmd, function(\Animo\BuildahSDK\BuildahTask $task) {
    $task->command("echo Hello from the git container!");
});

// Add git helper task to pipeline; clone a project to a directory
$pipeline->gitTask('clone project')->clone(
    "https://github.com/laravel/laravel.git",
    "/buildah/project",
    "8.x",
    $gitContainer
);

// New pipeline task: Create a composer container and install composer dependencies
$pipeline->task('composer install', function(\Animo\BuildahSDK\BuildahTask $task) {
    $task->command("cd /buildah/project && composer install")->setTimeout(3600);
}, new \Animo\BuildahSDK\BuildahContainer("composer:latest", "composer-container"));

// Execute the pipeline tasks
$pipeline->execute();

// Build the laravel image from php
$laravelContainerFile = new Containerfile();
$laravelContainerFile->compose(function (Instructions $instructions) {
    $instructions->addLine('FROM webdevops/php-nginx:8.0');
    $instructions->addLine('ENV WEB_DOCUMENT_ROOT /app/public');
    $instructions->addLine('RUN rm -rf /app');
    $instructions->addLine('RUN cp -ar /buildah/project/. /app');
    $instructions->addLine('RUN chown -R application:application /app');
});

$imageName = $buildahCmd->build('animo/laravel-php', $laravelContainerFile);

// Run post-build commands (setup laravel)
$appContainer = new \Animo\BuildahSDK\BuildahContainer($imageName, "laravel-app");
$postBuildPipeline = new \Animo\BuildahSDK\Pipeline(new \Animo\BuildahSDK\BuildahCmd([]));
$postBuildPipeline->task('init laravel', function(\Animo\BuildahSDK\BuildahTask $task) {
    $script = $task->script("app key generation");
    $script->addLine("cd /app");
    $script->addLine("cp .env.example .env");
    $script->addLine("php artisan key:generate");
}, $appContainer);

$postBuildPipeline->execute();

// Commit changes back in to the laravel image
$appContainer->commit($buildahCmd);
$appContainer->dockerArchive($buildahCmd, '/builds/jd-laravel-latest.tar', 'jonathandey/laravel:latest');
// $appContainer->push('jonathandey/laravel:latest');

