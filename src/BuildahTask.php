<?php

namespace Animo\BuildahSDK;

use Animo\BuildahSDK\Contracts\TaskStep;
use Animo\BuildahSDK\Task\Command;
use Animo\BuildahSDK\Task\Script;

class BuildahTask {

    /**
     * @var Pipeline $pipeline
     */
    private $pipeline;

    /**
     * @var array $steps
     */
    private $steps = [];

    /**
     * @var BuildahContainer $container
     */
    private $container;

    /**
     * @var bool
     */
    private $executed = false;

    /**
     * BuildahTask constructor.
     * @param Pipeline $pipeline
     */
    public function __construct(\Animo\BuildahSDK\Pipeline $pipeline)
    {
        $this->pipeline = $pipeline;
    }

    /**
     * @param string $command
     * @return Command
     */
    public function command(string $command, $alias = null) : Command
    {
        $command = new Command($command);

        $this->addStep($command, $alias);

        return $command;
    }

    public function script($alias = null) : Script
    {
        $script = new Script();

        $this->addStep($script, $alias);

        return $script;
    }

    public function addStep(TaskStep $step, $alias = null)
    {
        if (! is_null($alias)) {
            $this->steps[$alias] = $step;
        } else {
            $this->steps[] = $step;
        }
    }

    /**
     * @param BuildahContainer $container
     */
    public function in(BuildahContainer $container)
    {
        $this->container = $container;
    }

    /**
     * @return BuildahContainer
     */
    public function getContainer(): BuildahContainer
    {
        return $this->container;
    }

    /**
     * @return array
     */
    public function getSteps(): array
    {
        return $this->steps;
    }

    /**
     * @return TaskStep|null
     */
    public function lastStep(): ?TaskStep
    {
        $lastStepKey = array_key_last($this->steps);

        if (isset($this->steps[$lastStepKey])) {
            return $this->steps[$lastStepKey];
        }

        return null;
    }

    public function execute(BuildahCmd $buildahCmd)
    {
        $steps = $this->getSteps();
        $container = $this->getContainer();

        foreach ($steps as $stepAlias => $step) {

            $command = $step->getRunCommand($container);

            $process = $container->run(
                $buildahCmd,
                $command,
                $step->getTimeout()
            );

            $step->setProcess($process);

            if (! $process->isSuccessful()) {
                throw new \Exception("Step '$stepAlias' failed. [{$process->getErrorOutput()}]");
            }
        }

        $this->executed = true;
    }

    public function hasExecuted()
    {
        return $this->executed;
    }
}
