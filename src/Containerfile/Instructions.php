<?php

namespace Animo\BuildahSDK\Containerfile;

class Instructions {

    /**
     * @var array $lines
     */
    private $lines = [];

    public function addLine(string $line)
    {
        $this->lines[] = $line;
    }

    /**
     * @return array
     */
    public function getLines(): array
    {
        return $this->lines;
    }
}
