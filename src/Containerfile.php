<?php

namespace Animo\BuildahSDK;

use Animo\BuildahSDK\Containerfile\Instructions;

class Containerfile {

    /**
     * @var Instructions $instructions
     */
    private $instructions;

    public function compose(Callable $callback)
    {
        $this->instructions = new Instructions();

        $callback($this->instructions);

        return $this;
    }

    public function toString()
    {
        $instructionLines = $this->instructions->getLines();
        $lines = "";

        foreach ($instructionLines as $line) {
            $lines .= "$line\n";
        }

        return $lines;
    }

    public function __toString()
    {
        return $this->toString();
    }
}
