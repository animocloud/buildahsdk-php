<?php

namespace Animo\BuildahSDK\System\Files;

class Temporary {

    public function create(string $contents)
    {
        $tmpfile = static::createTmpFile();
        $tmpfilePath = $tmpfile['uri'];

        file_put_contents($tmpfilePath, $contents);

        return $tmpfilePath;
    }

    private static function createTmpFile()
    {
        $tmpfile = tmpfile();

        return stream_get_meta_data($tmpfile);
    }
}
