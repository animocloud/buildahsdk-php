<?php

namespace Animo\BuildahSDK\System;

use Animo\BuildahSDK\System\Files\Temporary;

class FileManager {

    public static function temp()
    {
        return new Temporary();
    }
}
