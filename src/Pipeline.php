<?php

namespace Animo\BuildahSDK;

use Animo\BuildahSDK\Pipeline\Git;

class Pipeline {

    /**
     * @var BuildahCmd $buildah
     */
    private $buildah;

    /**
     * @var array $tasks
     */
    private $tasks = [];

    /**
     * @var array $containers
     */
    private $containers;

    /**
     * Pipeline constructor.
     * @param BuildahCmd $buildah
     */
    public function __construct(BuildahCmd $buildah)
    {
        $this->buildah = $buildah;
    }

    /**
     * @param $alias
     * @return Git
     */
    public function gitTask($alias): Git
    {
        return new Git($alias, $this);
    }

    public function task($alias, Callable $callback, BuildahContainer $container)
    {
        $task = new BuildahTask($this);

        $callback($task);

        $task->in($container);

        $this->addTask($alias, $task);

        return $this;
    }

    /**
     * @param $alias
     * @return BuildahTask|null
     */
    public function getTask($alias)
    {
        if (isset($this->tasks[$alias])) {
            return $this->tasks[$alias];
        }

        return null;
    }

    public function execute()
    {
        foreach ($this->tasks as $task) {

            $container = $task->getContainer();
            if (! $container->hasStarted()) {
                $this->startContainer($container);
            }

            $task->execute($this->buildah);
        }
    }

    private function startContainer($container)
    {
        $this->containers[] = $container;
        $container->start($this->buildah);
    }

    public function addTask($alias, BuildahTask $task)
    {
        $this->tasks[$alias] = $task;
    }
}
