<?php

namespace Animo\BuildahSDK;

use Animo\BuildahSDK\Containerfile\Instructions;
use Symfony\Component\Yaml\Yaml;

class BuildahCiFactory {

    /**
     * @var string[]
     */
    private static $globalKeywords = ['version', 'variables', 'volumes'];

    /**
     * @var BuildahCmd|null $buildahCmd
     */
    private static $buildahCmd = null;

    /**
     * @var array
     */
    private static $containers = [];

    /**
     * @var bool
     */
    private static $pendingPush = false;

    /**
     * @var array
     */
    private static $globalVariables = [];

    /**
     * These should only be set once, before the firs container run.
     * They're then used to reset the $globalVariables back to a clean state
     *
     * @var array
     */
    private static $staticGlobalVariables = [];

    /**
     * @param string $filePath
     * @throws \Exception
     */
    public static function fromYaml(string $filePath)
    {
        if (! file_exists($filePath)) {
            throw new \Exception("Missing file '$filePath'");
        }

        $ciValues = Yaml::parseFile($filePath);

        if (isset($ciValues['variables']) && is_array($ciValues['variables'])) {
            self::$staticGlobalVariables = self::$globalVariables = $ciValues['variables'];
        }

        self::$buildahCmd = self::initBuildahCmd($ciValues);
        self::$buildahCmd->cleanUp();

        foreach ($ciValues as $ciKeyword => $keywordValues) {

            if (in_array($ciKeyword, self::$globalKeywords)) {
                continue;
            }

            self::tearDownContainerVariables();
            self::setupContainerVariables($keywordValues);

            $pipeline = new Pipeline(self::$buildahCmd);
            $container = null;
            $containerName = static::fillTemplateVariables($ciKeyword);

            // dd($ciKeyword, $keywordValues);
            if (array_key_exists('image', $keywordValues)) {
                $container = self::makeContainer($containerName, $keywordValues);
            } elseif (array_key_exists('containerfile', $keywordValues)) {
                $container = self::buildContainer($containerName, $keywordValues);

                self::$pendingPush = $container;
            }

            if (is_null($container)) {
                throw new \Exception("No container available");
            }

            // this will run "before_script" if provided
            self::startContainer($container, $keywordValues);

            // Should not allow the use of "git" and "script"?
            if (array_key_exists('git', $keywordValues)) {
                self::addGitTask($pipeline, $container, $keywordValues);
            }

            if (array_key_exists('script', $keywordValues)) {
                self::script($pipeline, $container, $keywordValues);
            }

            if (array_key_exists('after_script', $keywordValues)) {
                self::afterScript($pipeline, $container, $keywordValues);
            }

            if (self::$pendingPush != false) {
                self::commitAndPush(self::$pendingPush);
            }

            try {
                $pipeline->execute();
            } catch (\Exception $e) {
                die("\nFailed at step $ciKeyword.\n" . $e->getMessage());
            }
        }
    }

    /**
     * @param array $ciValues
     * @return BuildahCmd
     */
    private static function initBuildahCmd(array $ciValues) : BuildahCmd
    {
        $options = [];

        if (isset($ciValues['volumes']) && is_array($ciValues['volumes'])) {
            foreach ($ciValues['volumes'] as $volume) {
                $options[] = "--volume=$volume";
            }
        }

        $buildahCmd = new BuildahCmd($options);

        return $buildahCmd;
    }

    /**
     * @param $name
     * @param $keywordValues
     * @return BuildahContainer
     */
    private static function makeContainer($name, $keywordValues) : BuildahContainer
    {
        $container = new BuildahContainer(
            $keywordValues['image'],
            $name
        );

        self::$containers[$name] = $container;

        return $container;
    }

    /**
     * @param $name
     * @param $keywordValues
     * @return BuildahContainer
     * @throws \Exception
     */
    private static function buildContainer($name, $keywordValues) : BuildahContainer
    {
        $containerfileConfig = $keywordValues['containerfile'];
        $instructionValues = $containerfileConfig['instructions'];

        $containerFile = new Containerfile();
        $containerFile->compose(function (Instructions $instructions) use ($instructionValues) {
            foreach ($instructionValues as $instructionValue) {
                $instructions->addLine(self::fillTemplateVariables($instructionValue));
            }
        });

        $imageName = self::$buildahCmd->build($containerfileConfig['image'], $containerFile);

        $container = new BuildahContainer($imageName, $name);

        return self::$containers[$name] = $container;
    }

    /**
     * @param BuildahContainer $container
     * @param $keywordValues
     * @throws \Exception
     */
    private static function startContainer(BuildahContainer $container, $keywordValues)
    {
        $command = isset($keywordValues['before_script']) ? $keywordValues['before_script'] : null;

        $container->start(self::$buildahCmd, function (BuildahTask $task) use ($command) {
            self::setupTaskCommand($task, $command);
        });
    }

    /**
     * @param Pipeline $pipeline
     * @param BuildahContainer $container
     * @param $keywordValues
     * @throws \Exception
     */
    private static function addGitTask(Pipeline $pipeline, BuildahContainer $container, $keywordValues)
    {
        $requiredVariables = ['git', 'name', 'git.repo', 'git.target'];

        if (! self::validateRequiredVariables($keywordValues, $requiredVariables)) {
            $variableList = implode(", ", $requiredVariables);
            throw new \Exception("Git task requires the variables: $variableList");
        }

        $gitConfig = $keywordValues['git'];
        $taskName = $keywordValues['name'];

        $gitBranch = (! isset($gitConfig['branch'])) ? 'master' : $gitConfig['branch'];
        // The directory to clone into
        $gitTarget = $gitConfig['target'];
        $gitRepo = $gitConfig['repo'];

        $pipeline->gitTask($taskName)->clone(
            self::fillTemplateVariables($gitRepo),
            self::fillTemplateVariables($gitTarget),
            self::fillTemplateVariables($gitBranch),
            $container
        );
    }

    /**
     * @param Pipeline $pipeline
     * @param BuildahContainer $container
     * @param $keywordValues
     * @throws \Exception
     */
    private static function script(Pipeline $pipeline, BuildahContainer $container, $keywordValues)
    {
        $requiredVariables = ['script', 'name'];

        if (! self::validateRequiredVariables($keywordValues, $requiredVariables)) {
            $variableList = implode(", ", $requiredVariables);
            throw new \Exception("Git task requires the variables: $variableList");
        }

        $taskName = $keywordValues['name'];
        $command = $keywordValues['script'];
        $commandTimeout = isset($keywordValues['script_timeout']) ? $keywordValues['script_timeout'] : 60;

        self::addTask($pipeline, $container, $taskName, $command, $commandTimeout);
    }

    /**
     * @param Pipeline $pipeline
     * @param BuildahContainer $container
     * @param $keywordValues
     * @throws \Exception
     */
    private static function afterScript(Pipeline $pipeline, BuildahContainer $container, $keywordValues)
    {
        $requiredVariables = ['after_script', 'name'];

        if (! self::validateRequiredVariables($keywordValues, $requiredVariables)) {
            $variableList = implode(", ", $requiredVariables);
            throw new \Exception("Git task requires the variables: $variableList");
        }

        $taskName = $keywordValues['name'];
        $command = $keywordValues['after_script'];
        $commandTimeout = isset($keywordValues['script_timeout']) ? $keywordValues['script_timeout'] : 60;

        self::addTask($pipeline, $container, $taskName, $command, $commandTimeout);
    }

    /**
     * @param Pipeline $pipeline
     * @param BuildahContainer $container
     * @param $taskName
     * @param $command
     * @param int $commandTimeout
     */
    private static function addTask(Pipeline $pipeline, BuildahContainer $container, $taskName, $command, $commandTimeout = 60)
    {
        $pipeline->task($taskName, function (BuildahTask $task) use ($command, $commandTimeout) {
            self::setupTaskCommand($task, $command, $commandTimeout);
        }, $container);
    }

    /**
     * @param BuildahContainer $container
     * @throws \Exception
     */
    private static function commitAndPush(BuildahContainer $container)
    {
        $container->commit(self::$buildahCmd);
        // @todo push
    }

    /**
     * @param string $template
     * @param array $variables
     * @return string|string[]
     */
    private static function fillTemplateVariables(string $template)
    {
        $variables = self::$globalVariables;

        // @todo Can this be optimised anymore?
        foreach ($variables as $variable => $value) {
            $template = str_replace("$".$variable, $value, $template);
        }

        return $template;
    }

    /**
     * @param BuildahTask $task
     * @param $command
     * @param int $commandTimeout
     */
    private static function setupTaskCommand(BuildahTask $task, $command, $commandTimeout = 60)
    {
        if (! is_null($command) && is_array($command)) {
            $script = $task->script();
            foreach ($command as $commandLine) {
                $script->addLine(self::fillTemplateVariables($commandLine))->setTimeout($commandTimeout);
            }
        } elseif (! is_null($command)) {
            $task->command(self::fillTemplateVariables($command))->setTimeout($commandTimeout);
        }
    }

    /**
     * @param $keywordValues
     * @param array $requiredVariables
     * @return bool
     */
    private static function validateRequiredVariables($keywordValues, array $requiredVariables)
    {
        foreach ($requiredVariables as $requiredVariable) {

            $requiredVariableParts = explode(".", $requiredVariable);

            // When there are sub parts (down 1 level) to this, check those exists too.
            // E.g. git.repo - will check the "repo" key exists within the "git" key
            if (count($requiredVariableParts) == 1 && ! array_key_exists($requiredVariable, $keywordValues)) {
                return false;
            } elseif (count($requiredVariableParts) > 1) {
                // If the key is not an array and we are checking for parts, make an empty array (there is likely an issue already)
                $subKeywordValues = (is_array($keywordValues[$requiredVariableParts[0]])) ? $keywordValues[$requiredVariableParts[0]] : [];
                $result = self::validateRequiredVariables($subKeywordValues, [$requiredVariableParts[1]]);

                if (! $result) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Add container variables to the global variables array
     *
     * @param array $keywordValues
     */
    private static function setupContainerVariables(array $keywordValues)
    {
        $containerVariables = (isset($keywordValues['variables']) && is_array($keywordValues['variables'])) ?
            $keywordValues['variables'] : []
        ;

        self::$globalVariables = array_merge_recursive(self::$globalVariables, $containerVariables);
    }

    /**
     * Reset global variables back to their original state
     */
    private static function tearDownContainerVariables()
    {
        self::$globalVariables = self::$staticGlobalVariables;
    }
}
