<?php

namespace Animo\BuildahSDK;

use Animo\BuildahSDK\System\FileManager;
use Symfony\Component\Process\Process;

class BuildahCmd {

    public const BUILDAH_CMD = 'buildah';

    /**
     * @var array $options
     */
    private $options;

    /**
     * BuildahCmd constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->options = $options;
    }

    /**
     * @param array $command
     * @return Process
     */
	public function from(array $command) : Process
	{
        $command = array_merge([self::BUILDAH_CMD, "from"], $command);

		return static::newProcess($command);
	}

    /**
     * @param array $command
     * @return Process
     */
    public function run(array $command) : Process
    {
        $command = array_merge(
            [self::BUILDAH_CMD, "run"],
            $this->options,
            $command
        );

        return static::newProcess($command);
    }

    /**
     * @param array $command
     * @return Process
     */
    public function rm(array $command) : Process
    {
        $command = array_merge(
            [self::BUILDAH_CMD, 'rm'],
            $command
        );

        return static::newProcess($command);
    }

    /**
     * @param array $command
     * @return Process
     */
    public function commit(array $command) : Process
    {
        $command = array_merge([self::BUILDAH_CMD, 'commit'], $command);

        return static::newProcess($command);
    }

    /**
     * @param array $command
     * @return Process
     */
    public function push(array $command) : Process
    {
        $command = array_merge([self::BUILDAH_CMD, 'push'], $command);

        return static::newProcess($command);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function cleanUp()
    {
        $this->cleanUpContainers();
        $this->cleanUpImages();
    }

    /**
     * @param array $command
     * @return Process
     */
    protected static function newProcess(array $command)
    {
        return new Process($command, null, null, null, 300);
    }

    public function build(string $imageName, Containerfile $containerfile)
    {
        $containerFilePath = FileManager::temp()->create((string) $containerfile);

        $command = array_merge(
            [self::BUILDAH_CMD, 'bud'],
            $this->options,
            ['--layers', '-f', $containerFilePath, '-t', $imageName]
        );

        $process = static::newProcess($command);
        $process->setTimeout(300);
        $process->run(function($type, $buffer) {
            static::output($type, $buffer);
        });

        if (! $process->isSuccessful()) {
            throw new \Exception("Failed to build '$imageName' image. [{$process->getErrorOutput()}]");
        }

        return $imageName;
    }

    public static function output($type, $buffer)
    {
        echo $buffer;
    }

    private function cleanUpContainers()
    {
        $process = $this->rm(["--all"]);
        $process->run(function($type, $buffer) {
            static::output($type, $buffer);
        });

        if (! $process->isSuccessful()) {
            throw new \Exception("Failed to cleanup buildah containers");
        } else {
            return true;
        }
    }

    private function cleanUpImages()
    {
        $process = static::newProcess([self::BUILDAH_CMD, 'rmi', '--all']);
        $process->run(function($type, $buffer) {
            static::output($type, $buffer);
        });

        if (! $process->isSuccessful()) {
            throw new \Exception("Failed to cleanup buildah images");
        } else {
            return true;
        }
    }

}
