<?php

namespace Animo\BuildahSDK\Pipeline;

use Animo\BuildahSDK\BuildahContainer;
use Animo\BuildahSDK\Pipeline;

class Git {

    public const GIT_IMAGE = 'alpine/git';

    /**
     * @var string $taskAlias
     */
    private $taskAlias;

    /**
     * @var Pipeline $pipeline
     */
    private $pipeline;

    /**
     * Git constructor.
     * @param string $taskAlias
     * @param Pipeline $pipeline
     */
    public function __construct(string $taskAlias, Pipeline $pipeline)
    {
        $this->taskAlias = $taskAlias;
        $this->pipeline = $pipeline;
    }

    public function clone(string $gitRepo, string $targetDir, $branchName = 'master', BuildahContainer $container = null)
    {
        if (is_null($container)) {
            $container = $this->setupContainer();
        }

        $this->pipeline->task($this->taskAlias, function(\Animo\BuildahSDK\BuildahTask $task) use ($gitRepo, $targetDir, $branchName) {
            $script = $task->script('clone git project');
            $script->addLine("mkdir -p $targetDir");
            $script->addLine("git clone -b $branchName --recursive $gitRepo $targetDir");
            // $script->addLine("git init");
            // $script->addLine("git remote add origin $gitRepo");
            // $script->addLine("git fetch origin");
            // $script->addLine("git checkout -b $branchName --track origin/$branchName");
        }, $container);

        return $this->pipeline;
    }

    private function setupContainer()
    {
        return new BuildahContainer(self::GIT_IMAGE, "git-container");
    }
}
