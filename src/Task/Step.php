<?php

namespace Animo\BuildahSDK\Task;

use Symfony\Component\Process\Process;

abstract class Step {

    /**
     * @var Process $process
     */
    protected $process;

    /**
     * @var int $timeout
     */
    protected $timeout = 60;

    /**
     * @return string
     */
    abstract public function asString(): string;

    /**
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * @param int $timeout
     */
    public function setTimeout(int $timeout)
    {
        $this->timeout = $timeout;
    }

    /**
     * @return Process
     */
    public function getProcess(): Process
    {
        if (is_null($this->process)) {
            throw new \Exception("Process isn't set. Have you executed the pipeline?");
        }

        return $this->process;
    }

    /**
     * @param Process $process
     */
    public function setProcess(Process $process)
    {
        $this->process = $process;
    }

    public function __toString()
    {
        $this->asString();
    }
}
