<?php

namespace Animo\BuildahSDK\Task;

use Animo\BuildahSDK\BuildahContainer;
use Animo\BuildahSDK\Contracts\TaskStep;

class Command extends Step implements TaskStep {

    /**
     * @var string $command
     */
    private $command;

    /**
     * BuildahStep constructor.
     * @param string $command
     */
    public function __construct(string $command)
    {
        $this->command = $command;
    }

    /**
     * @param BuildahContainer $container
     * @return string
     */
    public function getRunCommand(BuildahContainer $container): string
    {
        return $this->asString();
    }

    /**
     * @return string
     */
    public function asString(): string
    {
        return $this->command;
    }

}
