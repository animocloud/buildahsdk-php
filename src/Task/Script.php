<?php

namespace Animo\BuildahSDK\Task;

use Animo\BuildahSDK\BuildahContainer;
use Animo\BuildahSDK\Contracts\TaskStep;
use Animo\BuildahSDK\System\FileManager;

class Script extends Step implements TaskStep {

    private $lines = [];

    public function addLine(string $line)
    {
        $this->lines[] = $line;

        return $this;
    }

    /**
     * @return array
     */
    public function getLines(): array
    {
        return $this->lines;
    }

    public function asString(): string
    {
        $output = "";

        foreach ($this->lines as $line) {
            $output .= "$line\n";
        }

        return $output;
    }

    /**
     * @param BuildahContainer $container
     * @return string
     */
    public function getRunCommand(BuildahContainer $container): string
    {
        $scriptFilePath = $this->createScriptFile();
        $scriptMountPath = $container->mountScript($scriptFilePath);

        return $scriptMountPath;
    }

    private function createScriptFile() : string
    {
        $scriptFile = FileManager::temp()->create($this->asString());

        chmod($scriptFile, '0700');

        return $scriptFile;
    }


}
