<?php

namespace Animo\BuildahSDK;

use Symfony\Component\Process\Process;

class BuildahContainer {

    /**
     * @var string $image
     */
    private $image;

    /**
     * @var string $name
     */
    private $name;

    /**
     * @var bool $started
     */
    private $started = false;

    /**
     * @var array $arguments
     */
    private $arguments = [];

    /**
     * BuildahContainer constructor.
     * @param string $image
     * @param string $name
     */
    public function __construct(string $image, string $name)
    {
        $this->image = $image;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function hasStarted()
    {
        return $this->started;
    }

    public function start(BuildahCmd $buildahCmd, Callable $onStart = null)
    {
        $process = $buildahCmd->from(["--name=" . $this->name, $this->image]);
        $process->run(function($type, $buffer) use ($buildahCmd) {
            $buildahCmd::output($type, $buffer);
        });

        if ($process->isSuccessful()) {
            $this->started = true;

            // If an onStart task has been provided, run it now
            if (! is_null($onStart)) {
                $pipeline = new Pipeline($buildahCmd);
                $task = new BuildahTask($pipeline);
                $task->in($this);

                $onStart($task);
                $pipeline->addTask("{$this->name} on start", $task);
                $pipeline->execute();
            }
        } else {
            throw new \Exception("Container '{$this->name}' failed to start.");
        }
    }

    /**
     * @param BuildahCmd $buildahCmd
     * @param string $command
     * @return Process
     */
    public function run(BuildahCmd $buildahCmd, string $command, $timeout = 60) : Process
    {
        $runCommand = array_merge(
            ['--tty=false'],
            $this->arguments,
            [
                $this->name,
                '--',
                'sh',
                '-c',
                $command
            ]
        );

        $process =  $buildahCmd->run($runCommand);

        $process->setTimeout($timeout);
        $process->run(function($type, $buffer) use ($buildahCmd) {
            $buildahCmd::output($type, $buffer);
        });

        return $process;
    }

    public function rm(BuildahCmd $buildahCmd)
    {
        $process = $buildahCmd->rm([$this->name]);
        $process->run(function($type, $buffer) use ($buildahCmd) {
            $buildahCmd::output($type, $buffer);
        });

        if (! $process->isSuccessful()) {
            throw new \Exception(
                "Container '{$this->name}' failed to be removed. [{$process->getErrorOutput()}]"
            );
        }
    }

    public function commit(BuildahCmd $buildahCmd, $imageName = null)
    {
        if (is_null($imageName)) {
            $imageName = $this->image;
        } else {
            $this->image = $imageName;
        }

        $process = $buildahCmd->commit([$this->name, $imageName]);
        $process->run(function($type, $buffer) use ($buildahCmd) {
            $buildahCmd::output($type, $buffer);
        });

        if (! $process->isSuccessful()) {
            throw new \Exception(
                "Container '{$this->name}' failed to be commit to '$imageName'. [{$process->getErrorOutput()}]"
            );
        }
    }

    public function push(string $remote)
    {

    }

    /**
     * Docker does not support loading or importing these archives, see dockerArchive instead
     *
     * @param BuildahCmd $buildahCmd
     * @param string $path
     * @param string $tag
     * @throws \Exception
     */
    public function ociArchive(BuildahCmd $buildahCmd, string $path, string $tag)
    {
        $process = $buildahCmd->push([$this->image, "oci-archive:$path:$tag"]);
        $process->run(function($type, $buffer) use ($buildahCmd) {
            $buildahCmd::output($type, $buffer);
        });

        if (! $process->isSuccessful()) {
            throw new \Exception(
                "Container '{$this->name}' failed to be commit to '{$this->image}'. [{$process->getErrorOutput()}]"
            );
        }
    }

    /**
     * Should be imported in to docker using "docker image load -i FILE.tar"
     *
     * @param BuildahCmd $buildahCmd
     * @param string $path
     * @param string $tag
     * @throws \Exception
     */
    public function dockerArchive(BuildahCmd $buildahCmd, string $path, string $tag)
    {
        $process = $buildahCmd->push([$this->image, "docker-archive:$path:$tag"]);
        $process->run(function($type, $buffer) use ($buildahCmd) {
            $buildahCmd::output($type, $buffer);
        });

        if (! $process->isSuccessful()) {
            throw new \Exception(
                "Container '{$this->name}' failed to be commit to '{$this->image}'. [{$process->getErrorOutput()}]"
            );
        }
    }

    public function mountScript(string $scriptFilePath)
    {
        $scriptName = "/scripts/run.sh";
        $this->arguments['script'] = "--volume=$scriptFilePath:$scriptName";

        return $scriptName;
    }
}
