<?php

namespace Animo\BuildahSDK\Contracts;

use Animo\BuildahSDK\BuildahContainer;
use Symfony\Component\Process\Process;

interface TaskStep {

    /**
     * @return int
     */
    public function getTimeout(): int;
    /**
     * @param int $timeout
     */
    public function setTimeout(int $timeout);

    /**
     * @return Process
     */
    public function getProcess(): Process;

    /**
     * @param Process $process
     */
    public function setProcess(Process $process);

    /**
     * @param BuildahContainer $container
     * @return string
     */
    public function getRunCommand(BuildahContainer $container): string;
}
