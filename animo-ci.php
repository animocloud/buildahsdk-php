<?php

use Animo\BuildahSDK\BuildahCiFactory;

require_once __DIR__ . "/vendor/autoload.php";

BuildahCiFactory::fromYaml("/spec/animo-ci.yml");
