FROM quay.io/buildah/stable:latest

RUN dnf -y update && \
	yum install -y dnf-plugins-core && \
	dnf -y install https://rpms.remirepo.net/fedora/remi-release-33.rpm && \
	dnf config-manager --set-enabled remi && \
	dnf module reset php && \
	dnf -y module install php:remi-7.4

COPY . /app

VOLUME ["/buildah", "/artifacts"]

WORKDIR /app

CMD ["php", "-f", "animo-ci.php"]
